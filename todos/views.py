from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoForm


# Create your views here.
def todo_list_update(request, id):
    todo_change = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_change)
        if form.is_valid():
            todo_change = form.save()
            return redirect("todo_list_detail", id=todo_change.id)
    else:
        form = TodoForm(instance=todo_change)
        context = {"form": form, "todo_ch": todo_change}
        return render(request, "todos edit html ")
    return render(request, "todos/edit.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todo_list = form.save(False)
            todo_list.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoForm()
        context = {
            "form": form,
        }
        return render(request, "todos/create.html", context)


def todo_list_detail(request, id):
    todo_item = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todo_item,
    }
    return render(request, "todos/details.html", context)


def todo_list_list(request):
    todolist = TodoList.objects.all()
    context = {
        "todo_list_list": todolist,
    }
    return render(request, "todos/list.html", context)
